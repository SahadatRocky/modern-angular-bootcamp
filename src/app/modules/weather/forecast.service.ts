import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { filter, mergeMap, pluck, switchMap, toArray } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ForecastService {
  private url = "https://openweathermap.org/data/2.5/forecast";
  constructor(private http: HttpClient) { }

  getForecast(){
    return this.getCurrentLocation()
      .pipe(
        map(coords => {
            return new HttpParams()
              .set('lat', String(coords.latitude))
              .set('lon', String(coords.longitude))
              .set('units', 'metric')
              .set('appid', '04a67e36f92b90559f7726293808788c')
        }),
        switchMap((params) => this.http.get(this.url,{params})),
        pluck('list'),
        mergeMap((value: any) => of(...value)),
        filter((value, index) => index % 8 == 0),
        map(value => {
          return{
            dataString: value.dt_txt,
            temp: value.temp
          }
        }),
        toArray()
      )
  }

  getCurrentLocation(){
    return new Observable<any>((observer)=>{
      window.navigator.geolocation.getCurrentPosition((position) =>{
        observer.next(position.coords);
        observer.complete();
      },
      (err)=>{
        observer.error(err);
      });
    })
  }
}
