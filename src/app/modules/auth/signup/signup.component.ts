import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { MatchPassword } from '../validators/match-password';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  authForm:any = FormGroup;

  constructor(private fb: FormBuilder,private matchPassword: MatchPassword,private authService: AuthService, private router: Router) {
    this.createForm();
  }

  ngOnInit(): void {
  }

  createForm(){
    this.authForm = this.fb.group({
      username: new FormControl('', [Validators.required,Validators.minLength(3),Validators.maxLength(20),Validators.pattern(/^[a-zA-Z0-9]+$/)]),
      password: new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(20)]),
      passwordConfirmation: new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(20)])

    },{ validators: [this.matchPassword.validate] }
    );
  }

  onSubmit(){
    if(this.authForm.invalid){
      return;
    }
    this.authService.signup(this.authForm.value).subscribe((Response) => {
       console.log(Response);
       //navigate to some other route
       this.router.navigateByUrl('/inbox');
    }, (error)=>{
        if(!error.status){
          this.authForm.setErrors({noConnection: true});
        }else{
          this.authForm.setErrors({unknownError: true});
        }
    });

  }

}
