import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { MatchPassword } from '../validators/match-password';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  authForm:any = FormGroup;
  constructor(private fb: FormBuilder,private matchPassword: MatchPassword,private authService: AuthService, private router: Router) {
    this.createForm();
  }

  ngOnInit(): void {
  }

  createForm(){
    this.authForm = this.fb.group({
      username: new FormControl('', [Validators.required,Validators.minLength(3),Validators.maxLength(20),Validators.pattern(/^[a-zA-Z0-9]+$/)]),
      password: new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(20)])
    },{ validators: [this.matchPassword.validate] }
    );
  }

  onSubmit(){
    if(this.authForm.invalid){
      return;
    }
    this.authService.signin(this.authForm.value).subscribe((Response) => {
       console.log(Response);
       //navigate to some other route
       this.router.navigateByUrl('/inbox');
    }, (error)=>{
        console.log(error);
        if(error.username || error.password){
            this.authForm.setErrors({ credentials: true });
        }
    });

  }

}
