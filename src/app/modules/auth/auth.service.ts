import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
interface SignupCredentials{
  username: string;
  password: string;
  passwordConfirmation: string;
}

interface SigninCredentials{
  username: string;
  password: string;
}

interface SignupResponse{
  username: string;
}

interface SignedinResponse{
  authenticated: boolean;
  username: string;
}




@Injectable({
  providedIn: 'root'
})
export class AuthService {
  rootUrl = "https://api.angular-email.com";
  signedin$: any = new BehaviorSubject<any>(false);
  constructor(private http: HttpClient) { }

  signup(credentials: SignupCredentials){
    return this.http.post<SignupResponse>(`${this.rootUrl}/auth/signup`,
      credentials
      ).pipe(
        map(()=>{
          this.signedin$.next(true);
        })
      );
  }

  checkAuth(){
    return this.http.get<SignedinResponse>(`${this.rootUrl}/auth/signedin`)
    .pipe(
      map(({authenticated})=>{
        this.signedin$.next(authenticated);
        console.log();
    }));
  }

  signout(){
    return this.http.post(`${this.rootUrl}/auth/signout`,{}).pipe(
      map(() => {
        this.signedin$.next(false);
      })
    )
  }

  signin(credentials: SigninCredentials){
    return this.http.post(`${this.rootUrl}/auth/signin`, credentials).pipe(
      map(() => {
        this.signedin$.next(true);
      })
    )
  }

}
