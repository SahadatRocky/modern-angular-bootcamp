import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authReq = request.clone({
      withCredentials: true,
      setHeaders: this.getHeaders()
    });
    console.log(authReq)
    return next.handle(authReq);
  }

  getHeaders(): any{
    return{
      'content-type': 'application/json',
      'cache-control': 'no-cache',
      'Access-Control-Allow-Origin': '*'
    }
 }

  /*
  getHeaders(): any{
    return{
      'content-type': 'application/json',
      // 'oauth-token': this.localStorageService.getOauthToken(),
      // 'oauth-data': this.localStorageService.getOauthData(),
      'request-type': 'APP_DASHBOARD',
      'device-id': '31241234235345',
      'client-id': '159617a323cdb0dfd8025814c2676f202c1e513b',
      'secret-key': 'e07082e4faae7d8e6bbe4cbb5680b6adf4460539',
      'cache-control': 'no-cache',
      'Access-Control-Allow-Origin': '*'
    }
 }
 */

}



