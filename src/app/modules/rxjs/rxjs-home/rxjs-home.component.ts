import { Component, OnInit } from '@angular/core';
import { bindCallback, empty, forkJoin, from, fromEvent, iif, interval, merge, of, throwError, timer } from 'rxjs';
import {
  catchError,
  concatMap,
  count,
  delay,
  distinct,
  distinctUntilChanged,
  distinctUntilKeyChanged,
  elementAt,
  endWith,
  every,
  exhaustMap,
  find,
  findIndex,
  first,
  flatMap,
  groupBy,
  isEmpty,
  last,
  map,
  max,
  mergeAll,
  mergeMap,
  min,
  pairwise,
  pluck,
  reduce,
  single,
  startWith,
  switchMap,
  take,
  throttleTime,
} from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { flatten } from '@angular/compiler';

interface Person{
   age:number;
   name:string;
}



@Component({
  selector: 'app-rxjs-home',
  templateUrl: './rxjs-home.component.html',
  styleUrls: ['./rxjs-home.component.css'],
})
export class RxjsHomeComponent implements OnInit {
  observable = of(1, 2, 3, 4, 5);
  constructor() {}

  ngOnInit(): void {
    // this.of();
    // this.callback();
    // this.from();
    // this.throttleTime();
    // this.distinct();
    // this.distinctUntilKeyChange();
    // this.merge();
    // this.forkJoin();
    // this.throwError();
    // this.mergeMaps();
    // this.switchMaps();
    // this.concatMaps();
    // this.exhaustMaps();
    // this.plucks();
       this.groupBys();
       this.pairwises();
    // this.ajax();
    // this.iif();
    // this.mergeMap();
  }

  pairwises(){
    of({a:1},{b:2},{c:3},{d:4},{e:5},{f:6},{g:7},{h:8},{i:9},{j:10}).pipe(
      pairwise()
    ).subscribe(data=> console.log(data));
  }


  groupBys(){
    of(
      {id: 1, name: 'JavaScript'},
      {id: 2, name: 'Parcel'},
      {id: 2, name: 'webpack'},
      {id: 1, name: 'TypeScript'},
      {id: 3, name: 'TSLint'}
    ).pipe(
      groupBy(p => p.id),
      mergeMap((group$) => group$.pipe(reduce((acc, cur) => [...acc, cur], []))),
    )
    .subscribe(p => console.log(p));
  }


  plucks(){
    of({v:1},{v:2},{v:3}).pipe(
      pluck('v')
    ).subscribe(data => console.log(data));
  }


  exhaustMaps(){
    of('x','y','z').pipe(
      exhaustMap(el => of(1,2).pipe(
        delay(2000),
        map(n => el + n)
      ))
    ).subscribe(data => console.log(data));
  }

  concatMaps(){
    of('x','y','z').pipe(
      concatMap(el => of(1,2).pipe(
        delay(2000),
        map(n => el + n)
      ))
    ).subscribe(data => console.log(data));
  }


  mergeMaps(){
    of('x','y','z').pipe(
      mergeMap(el => of(1,2).pipe(
        delay(2000),
        map(num=> el+num) // x1,x2,y1,y2,z1,z2
      ))
    ).subscribe(data => console.log(data));
  }

  switchMaps(){
    of('x','y','z').pipe(
      switchMap(el => of(1,2).pipe(
        delay(4000),
        map(num=> el+num)
      ))
    ).subscribe(data => console.log(data));
  }



  throwError(){
    interval(1000).pipe(
      mergeMap(x => x == 2 ? throwError("ERROR OCCUR!!"): of(1,2,3,4))
    ).subscribe(data => {
      console.log(data);
    })
  }



  forkJoin(){
    const observable = forkJoin({
      a: of(1,2,3,4),
      b: Promise.resolve(8),
      c: timer(4000)
    });

    observable.subscribe((value) => console.log(value));
  }


  merge(){
    const x =of(1,2,3,4,5);
    const y =of(10,20,30,40,50);
    merge(x,y).subscribe(data => console.log(data));
  }









  of() {
    this.observable
      .pipe(
        map((x) => x),
        take(3),
        // skip(3),
        last(),
        startWith(7),
        endWith(100),
        elementAt(2), //100
        startWith(17),
        startWith(27),
        startWith(37),
        find((x) => x % 5 === 0), //100
        isEmpty(), //false
        findIndex((x) => x % 2 === 0), // index - 0
        every((x) => x < 5), //true
        endWith(50),
        endWith(150),
        endWith(300),
        count(),
        endWith(250),
        endWith(450),
        endWith(30),
        reduce((prev, curr) => prev + curr),
        endWith(20),
        endWith(40),
        endWith(70),
        min(),
        endWith(80),
        endWith(120),
        endWith(60),
        max(),
        delay(2000),
        endWith(210),
        endWith(190),
        endWith(160)
      )
      .subscribe((data) => {
        console.log(data);
      });
  }

  iif() {
    iif(
      () => true,
      of(45, 34, 66, 55, 77),
      of(100, 23, 441, 123, 22)
    ).subscribe((data) => {
      console.log(data);
    });
  }

  mergeMap() {
    this.observable
      .pipe(
        mergeMap((value) => (value % 2 === 1 ? of(10, 20, 30, 40) : empty()))
      )
      .subscribe((data) => {
        console.log(data);
      });
  }

  someFunction = (cb) => {
    cb(5, 'some string', { someProperty: 'someValue' });
  };

  callback() {
    const bindFunction = bindCallback(this.someFunction);
    bindFunction().subscribe((data) => {
      console.log(data);
    });
  }

  ajax() {
    const users = ajax({
      url: 'https://httpbin.org/delay/2',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'rxjs-custom-header': 'Rxjs',
      },
      body: {
        rxjs: 'Hello World!',
      },
    }).pipe(
      map((response) => console.log('response: ', response)),
      catchError((error) => {
        console.log('error: ', error);
        return of(error);
      })
    );
  }

  from() {
    const result = from([10, 20, 30]);
    result.subscribe((x) => console.log(x));
  }

  throttleTime() {
    const clicks = fromEvent(document, 'click');
    const result = clicks.pipe(throttleTime(1000));
    result.subscribe((x) => console.log(x));
  }

  distinct() {
    of(1, 1, 2, 2, 3, 4, 4, 3, 3, 2, 1, 3, 3, 4)
      .pipe(
        map((x) => x),
        //  distinct(),  /// 1,2,3,4
        distinctUntilChanged()
      )
      .subscribe((data) => console.log(data));
  }


  distinctUntilKeyChange(){
      of<Person>(
        {age:12,name:"Rocky"},
        {age:22,name:"Rossi"},
        {age:33,name:"Rony"},
        {age:33,name:"Rony"},
        {age:33,name:"Rony"},
        {age:17,name:"Rushoo"},
        {age:17,name:"Rushoo"},
        {age:17,name:"Rushoo"},
        {age:17,name:"Rushoo"},
      ).pipe(
        map(x => x),
        distinctUntilKeyChanged('name')
      ).subscribe(data => console.log(data));
  }




}
function complete(arg0: () => void): (error: any) => void {
  throw new Error('Function not implemented.');
}

