import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-accordian',
  templateUrl: './accordian.component.html',
  styleUrls: ['./accordian.component.css']
})
export class AccordianComponent implements OnInit {

  @Input() data:any = [];
  openItemIndex = 0;
  constructor() { }

  ngOnInit(): void {
  }

  onClick(index: number) {
    if(index === this.openItemIndex){
      this.openItemIndex = -1;
    }else{
      this.openItemIndex = index;
    }
  }


}
