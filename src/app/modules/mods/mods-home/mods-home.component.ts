import { Component, OnInit } from '@angular/core';
import { WikipediaService } from '../service/wikipedia.service';

@Component({
  selector: 'app-mods-home',
  templateUrl: './mods-home.component.html',
  styleUrls: ['./mods-home.component.css']
})
export class ModsHomeComponent implements OnInit {

  openModal = false;
  pages:any = [];
  accrodianItems = [
    {
      title: 'why is the sky blue?',
      content: 'the sky is blue because it is'
  },
    {
      title: 'what does an orange taste like?',
      content: 'an orange tastes like an orange'
    },
    {
      title: 'what color is that cat?',
      content: 'the cat is an orange color'
    }
  ];
  constructor(private wikiService: WikipediaService) { }

  ngOnInit(): void {
  }

  onClick(){
    this.openModal = true;
  }

  onModalCloseClick(emitValue: boolean){
    this.openModal = emitValue;
  }

  onTerm(value:string){
    //console.log('----->>>', value);
    this.wikiService.Search(value).subscribe( (response : any) => {
        //console.log(response);
        this.pages = response.query.search;
        console.log(this.pages);
    });
  }

}
