import { Component, ElementRef, EventEmitter, OnInit, Output } from '@angular/core';
import * as $ from 'jquery'
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Output() close = new EventEmitter<boolean>();

  constructor(private el: ElementRef) { }

  ngOnInit(): void {
    document.body.appendChild(this.el.nativeElement);

  }

  onCloseClick(){
   this.close.emit(false);
  }

  ngOnDestroy(){
    this.el.nativeElement.remove();
  }

}
