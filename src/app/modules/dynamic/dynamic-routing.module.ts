import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DynamicHomeComponent } from './dynamic-home/dynamic-home.component';

const routes: Routes = [
  {path:'dynamic-home', component: DynamicHomeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DynamicRoutingModule { }
