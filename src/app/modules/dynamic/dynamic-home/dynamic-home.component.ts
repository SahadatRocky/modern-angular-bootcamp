import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {Input as InputField} from '../../shared/components/input/input';
@Component({
  selector: 'app-dynamic-home',
  templateUrl: './dynamic-home.component.html',
  styleUrls: ['./dynamic-home.component.css']
})
export class DynamicHomeComponent implements OnInit {

  field:InputField ={
    oid: "1",
    value: 50,
    tempValue: 50,
    key: "name",
    name: "name",
    nameEn: "name",
    nameBn: 'নাম',
    id: "1",
    label: "name",
    listLabel: "name",
    labelEn: "name",
    labelBn: "নাম",
    placeholder: "নাম",
    required: true,
    readOnly: true,
    visible: true,
    isDisabled: false,
    type: 'text',
    controlType: '',
    fieldWidth: 0,
    order: 0,
    canSearch: false,
    canColor: false,
    editableInList: false,
    isDefaultValueSet: false,
    changeDetect: [],
    isHiddenEventEmitter: false,
    clickable: false,
    textAlign: '',
    pattern: undefined,
    patternErrorMessage: '',
    toolTip: 'enter the name',
    min: '',
    max: '',
    minLength: 0,
    maxLength: 30,
    showToolTip: false,
    canShow: false,
    canSort: false,
    canTranslate: false,
    width: ''
  };

  myForm:any = FormGroup;
  toolBarData = [
    {name:'Home',href:"/dynamic/dynamic-home"},
    {name:'About',href:"/dynamic/dynamic-home"},
    {name:'Service',href:"/dynamic/dynamic-home"}
  ];
  constructor(private fb: FormBuilder) {
    this.createForm();
   }

   createForm(){
    this.myForm = this.fb.group({
      name : new FormControl('', [Validators.required,Validators.minLength(3),Validators.maxLength(20),Validators.pattern(/^[a-zA-Z0-9]+$/)])
    });
   }

  ngOnInit(): void {
    //set field with value
    //this.myForm.controls.name.setValue("Rocky");

    //set field with value
    //console.log(this.myForm.controls.name.value);
    //or
    //this.myForm.get('name').value

    //form field patch Value
    //   this.myForm.patchValue({
    //     name: 'SSSSSS'
    //  });
  }

  onSubmit(event:any){
   event.preventDefault();
    console.log(this.myForm.value);
  }

  emitedValue($event){
    console.log($event);
  }


}
