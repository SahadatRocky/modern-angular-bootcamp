import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DynamicRoutingModule } from './dynamic-routing.module';
import { DynamicHomeComponent } from './dynamic-home/dynamic-home.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    DynamicHomeComponent
  ],
  imports: [
    CommonModule,
    DynamicRoutingModule,
    SharedModule
  ]
})
export class DynamicModule { }
