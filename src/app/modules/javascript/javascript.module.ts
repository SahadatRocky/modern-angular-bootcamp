import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JavascriptRoutingModule } from './javascript-routing.module';
import { JavascriptHomeComponent } from './javascript-home/javascript-home.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    JavascriptHomeComponent
  ],
  imports: [
    CommonModule,
    JavascriptRoutingModule,
    SharedModule
  ]
})
export class JavascriptModule { }
