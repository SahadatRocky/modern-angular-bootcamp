import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JavascriptHomeComponent } from './javascript-home/javascript-home.component';

const routes: Routes = [
  {path:'javascript-home', component: JavascriptHomeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JavascriptRoutingModule { }
