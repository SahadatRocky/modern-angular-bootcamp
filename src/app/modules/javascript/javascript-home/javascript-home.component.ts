import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-javascript-home',
  templateUrl: './javascript-home.component.html',
  styleUrls: ['./javascript-home.component.css']
})
export class JavascriptHomeComponent implements OnInit {

  array = [1, 30, 39, 29, 10, 13];
  array1 = ['a', 'b', 'c'];
  array2 = ['d', 'e', 'f'];
  array3 = [0, 1, 2, [3, 4]];
  elements = ['Fire', 'Air', 'Water'];
  index =2;

  every:any;
  fill:any;
  filter:any;
  find:any;
  findIndex: any;
  includes:any;
  indexof:any;
  isArray:any;
  join:any;
  map1:any;
  pop:any;
  push:any;
  reduce:any;
  constructor() { }

  ngOnInit(): void {
       this.every = this.array.every((element)=> element < 50 );
       this.fill = this.array.fill(13,5);
       this.filter = this.array.filter((element)=> element<=30);
       this.find = this.array.find(element => element == 29);
       this.findIndex = this.array.findIndex((element) => element > 13);
       //this.array3.flat();[0,1,2,3,4]
       this.array1.forEach(element => console.log(element));//a b c
       Array.from('foo'); // [ "f", "o", "o" ]
       this.includes = this.array.includes(2);
       this.indexof = this.array1.indexOf('d');
      // this.isArray = this.array1.isArray(array1);

      this.join = this.elements.join();  //"Fire,Air,Water"
      this.elements.join(''); //"FireAirWater"
      this.elements.join('-'); //"Fire-Air-Water"

      this.array1.keys(); /// expected output: 0 // expected output: 1 // expected output: 2

      this.map1 = this.array.map(x => x * 2);
      this.pop = this.array.pop();
      this.push = this.array.push(100);

      this.reduce = this.array.reduce((previous, current) => previous + current);

      const animals = ['ant', 'bison', 'camel', 'duck', 'elephant'];

      console.log(animals.slice(2));
      // expected output: Array ["camel", "duck", "elephant"]

      console.log(animals.slice(2, 4));
      // expected output: Array ["camel", "duck"]

      console.log(animals.slice(1, 5));
      // expected output: Array ["bison", "camel", "duck", "elephant"]

      console.log(animals.slice(-2));
      // expected output: Array ["duck", "elephant"]

      console.log(animals.slice(2, -1));
      // expected output: Array ["camel", "duck"]

      const array = [1, 2, 3, 4, 5];

      // checks whether an element is even
      const even = (element) => element % 2 === 0;

      console.log(array.some(even));
      // expected output: true


      const months = ['Jan', 'March', 'April', 'June'];
      months.splice(1, 0, 'Feb');
      // inserts at index 1
      console.log(months);
      // expected output: Array ["Jan", "Feb", "March", "April", "June"]

      months.splice(4, 1, 'May');
      // replaces 1 element at index 4
      console.log(months);
      // expected output: Array ["Jan", "Feb", "March", "April", "May"]

      

  }

}
