import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn:'root'
})
export class DataStore{
  private _rowData:BehaviorSubject<any> = new BehaviorSubject({});
  public readonly rowdata = this._rowData.asObservable();
  constructor(){}

  setRowData(data:any){
    this._rowData.next(data);
  }

}
