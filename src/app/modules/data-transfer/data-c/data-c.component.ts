import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-c',
  templateUrl: './data-c.component.html',
  styleUrls: ['./data-c.component.css']
})
export class DataCComponent implements OnInit {
  receiveData:any = [];
  @Input() message:any;
  constructor() { }

  ngOnInit(): void {
  }

  sendData(data:any ){
     this.receiveData = data;
  }

}
