import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataCComponent } from './data-c.component';

describe('DataCComponent', () => {
  let component: DataCComponent;
  let fixture: ComponentFixture<DataCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataCComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
