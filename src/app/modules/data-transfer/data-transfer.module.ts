import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataTransferRoutingModule } from './data-transfer-routing.module';
import { DataAComponent } from './data-a/data-a.component';
import { DataBComponent } from './data-b/data-b.component';
import { DataCComponent } from './data-c/data-c.component';


@NgModule({
  declarations: [
    DataAComponent,
    DataBComponent,
    DataCComponent
  ],
  imports: [
    CommonModule,
    DataTransferRoutingModule
  ]
})
export class DataTransferModule { }
