import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataAComponent } from './data-a/data-a.component';
import { DataBComponent } from './data-b/data-b.component';
import { DataCComponent } from './data-c/data-c.component';

const routes: Routes = [
  {path:'a', component: DataAComponent},
  {path:'b', component: DataBComponent},
  {path:'c', component: DataCComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataTransferRoutingModule { }
