import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DataStore } from '../data.store';

@Component({
  selector: 'app-data-b',
  templateUrl: './data-b.component.html',
  styleUrls: ['./data-b.component.css']
})
export class DataBComponent implements OnInit {

  @Output() eventEmitter: EventEmitter<any> = new EventEmitter<any>();
  objList:any;
  constructor(private dataStore: DataStore) { }

  ngOnInit(): void {
      this.dataStore.rowdata.subscribe(res => {
        this.eventEmitter.emit(res);
        //console.log(res);
      });
  }

}
