import { Component, Input, OnInit } from '@angular/core';
import { DataStore } from '../data.store';

@Component({
  selector: 'app-data-a',
  templateUrl: './data-a.component.html',
  styleUrls: ['./data-a.component.css']
})
export class DataAComponent implements OnInit {
  message:string = 'hu ha ha ha';
  obj =[{
    oid:1,
    name:"sa-1",
    dept:"cse",
    empid: 2
  },
  {
    oid:2,
    name:"sa-2",
    dept:"cse",
    empid: 3
  }
];

  constructor(private dataStore: DataStore) { }

  ngOnInit(): void {
    this.dataStore.setRowData(this.obj);
  }

}
