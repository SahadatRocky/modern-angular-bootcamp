import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-collections-home',
  templateUrl: './collections-home.component.html',
  styleUrls: ['./collections-home.component.css']
})
export class CollectionsHomeComponent implements OnInit {
  data = [
    {name:'james', age:24, job: 'Designer'},
    {name:'jills', age:27, job: 'Developer'},
    {name:'Donald', age:29, job: 'SQA'}
  ];

  headers = [
    {key:"name", label: "name"},
    {key:"age", label: "age"},
    {key:"job", label: "job"},
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
