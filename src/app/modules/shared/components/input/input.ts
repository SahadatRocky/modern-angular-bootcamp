import { MasterField } from "src/app/core/master-model/master-field";


export class Input extends MasterField {
  controlType = 'input';
  pattern: RegExp;
  patternErrorMessage: string;
  toolTip: string;
  min: string;
  max: string;
  minLength: number;
  maxLength: number;
  showToolTip: boolean;
  textLimit?: number;

  constructor(options: Partial<Input> = {}) {
    super(options);
    this.pattern = options.pattern || new RegExp('[\s\S]*');
    this.patternErrorMessage = options.patternErrorMessage || '';
    this.toolTip = options.toolTip || '';
    this.min = options.min || '';
    this.max = options.max || '';
    this.minLength = options.minLength || 0;
    this.maxLength = options.maxLength || 100;
    this.showToolTip = options.showToolTip || false;
    this.textLimit = options.textLimit || 50;
  }
}
