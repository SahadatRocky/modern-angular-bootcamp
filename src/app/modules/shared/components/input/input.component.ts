import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Input as InputField} from './input';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  @Input() field: InputField;
  @Input() form: FormGroup;
  @Input() disable: boolean;
  @Input() readOnly: boolean;
  @Input() visible: boolean;
  @Input() type:string;
  @Input() id:string;
  @Input() key:string;
  @Input() max:string;
  @Input() min:string;
  @Input() maxLength:string;
  @Input() minLength:string;
  @Input() toolTip:string;
  @Input() pattern:string;
  @Input() placeholder:string;
  @Input() required:boolean;


  @Input() control: any;
  @Input() label:string;
  @Input() inputType:string;

  constructor() {
  }

  // @Input()
  // set changePropagate(changePropagate: { keyToChange: string; isFormValueChanged: boolean, propertyToChange: string; valueToChange: any }) {
  //   if (changePropagate) {
  //     if (!changePropagate.isFormValueChanged) {
  //       this.field[changePropagate.propertyToChange] = changePropagate.valueToChange;
  //     } else {
  //       this.form.controls[this.field.key].setValue(changePropagate.valueToChange);
  //     }
  //   }
  // }

  ngOnInit(): void {
    console.log(this.control);
  }

  setAppearance(): any {
    return this.disable || this.readOnly ? 'none' : '';
  }

  showError(){
    const {dirty, touched, errors} = this.control;
    return dirty && touched && errors;
   }

}
