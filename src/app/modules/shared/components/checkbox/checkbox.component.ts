import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {
  list = [
        {
          key: "one",
          value: false
        },
        {
          key: "two",
          value:false
        },
        {
          key: "three",
          value:false
        },
        {
          key: "four",
          value:false
        },
        {
          key: "five",
          value:false
        }];

  constructor() { }

  ngOnInit(): void {
  }

  toggle(event){
    console.log(event.checked);
  }
}
