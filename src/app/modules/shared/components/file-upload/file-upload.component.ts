import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})


export class FileUploadComponent implements OnInit {

  // @Input() field: MasterFormField<string>;
  // @Input() form: FormGroup;
  @Input() disable: boolean;
  @Input() hidden: boolean;
  @Input() clicked: boolean;
  @Output() fileUploadEventEmitter = new EventEmitter<File>();
  disabled: boolean;
  fileToUpload: File = null;
  // employeeOid: string;
  fileOid: string;

  constructor(
              // private fileUploadService: FileManagementService,
              private dialog: MatDialog,
              private toaster : ToastrService
              ) {}

  ngOnInit() {

  }

  fileInput(files: FileList) {
    if (!files.length) {
      return;
    }
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
    const deservedFileSize = 1000000 * 1.1;
    if (this.fileToUpload.size > deservedFileSize ) {
      this.toaster.warning('ফাইলের আকার ১ এমবি এর চেয়ে কম হতে হবে।','',{
        timeOut: 3000
      });
      return;
    }
    this.fileUploadEventEmitter.emit(this.fileToUpload);
    const data = new FormData();
    data.append('file', this.fileToUpload);
    data.append('title', this.fileToUpload.name);
    data.append('description', 'employee file attachment');
    data.append('oid', '1');
    data.append('tag', 'HRM');
    data.append('createdBy', '1');
    // this.fileUploadService.uploadFile(data)
    //   .subscribe(result => {
    //     // if (result.data.status === 200) {
    //     this.fileOid = result.data[0]['fileOid'];
    //     this.form.controls[this.field.key].setValue(this.fileOid);
    //     // }

    //   });
  }

  getFileName(): string {
    if (this.fileOid) {
      return this.fileToUpload.name;
    }
    return '';
  }

  viewFile() {
    // this.fileUploadService.downloadFile(String(this.field.value))
    //   .subscribe((response: Blob) => {
    //     const isImage = response.type.includes('image');
    //     let fileName = 'file.pdf';
    //     if (isImage) {
    //       fileName = 'image.' + response.type.split('/')[1];
    //     }
    //     this.dialog.open(DialogviewComponent, {
    //       disableClose: true,
    //       width: isImage ? '40%' : '70%',
    //       minHeight: isImage ? '50vh' : '90vh',
    //       data: {
    //         formDefinition: '',
    //         form: '',
    //         formField: '',
    //         title: '',
    //         model: new File([response], fileName)
    //       }
    //     });
    //   });
  }
}
