import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
// import { NgxGalleryImage, NgxGalleryOptions } from 'ngx-gallery';
@Component({
  selector: 'app-pdf-viewer',
  templateUrl: './pdf-viewer.component.html',
  styleUrls: ['./pdf-viewer.component.css']
})
export class PdfViewerComponent implements OnInit {
  pdfUrl='https://gahp.net/wp-content/uploads/2017/09/sample.pdf';
  src: string;
  isImage: boolean;
  @Input() file: File;
  // galleryOptions: NgxGalleryOptions[] = [
  //   {
  //     imageSize: 'contain',
  //     thumbnails: false,
  //     previewArrows: false,
  //     previewZoom: true,
  //     imageArrows: false,
  //     thumbnailsArrows: false
  //   }
  // ];
  // galleryImage: NgxGalleryImage[];

  constructor() {
  }
  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('file') && changes['file'].currentValue) {
      this.getBase64(this.file).then((res: string) => {
        this.isImage = !this.file.name.split('.').pop().includes('pdf');
        this.src = this.isImage ? res.split(',').pop() : res;
        if (this.isImage) {
          this.getImage();
        }
      });
    }
  }

  getBase64(file: File) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  getImage() {
    const url = URL.createObjectURL(this.file);
    // this.galleryImage = [
    //   {
    //     small: url,
    //     medium: url,
    //     big: url
    //   }
    // ];
  }

}
