import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  @Input() data: any;
  @Input() color: string;
  @Input() label: string;
  constructor() { }

  ngOnInit(): void {
  }

}
