import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @Input() form : FormGroup
  @Input() inputType:string;
  @Input() buttonName:string;
  @Input() className: string;
  @Input() isDisable: boolean;
  @Input() readOnly: boolean;

  @Output() emmiter = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
  }

  onClickButton(){
      //console.log(this.form.value);
      //this.emmiter.emit(this.form.value);
  }


}
