import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slide-toggle',
  templateUrl: './slide-toggle.component.html',
  styleUrls: ['./slide-toggle.component.css']
})
export class SlideToggleComponent implements OnInit {
  isChecked =false;
  constructor() { }

  ngOnInit(): void {
  }

  onChange($event){
    console.log($event.checked);
    this.isChecked = $event.checked;
  }

}
