import { getTreeNoValidDataSourceError } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.css']
})
export class SnackbarComponent implements OnInit {
  durationInSeconds = 5;
  constructor(private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  openSnackBar(){
    let snackbarRef = this._snackBar.open("This is snackbar message!!!",'',{
      duration: this.durationInSeconds * 1000,
      // verticalPosition: 'top',
      panelClass: ['reject']
    });

    // snackbarRef.afterDismissed().subscribe(()=>{
    //   console.log('after dismissed!!!');
    // });


    // snackbarRef.onAction().subscribe(()=>{
    //   console.log('on action clicked!!!');
    // });

  }

}
