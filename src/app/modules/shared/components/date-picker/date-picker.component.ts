import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.css']
})
export class DatePickerComponent implements OnInit {

  minDate = new Date();
  maxDate = new Date(2021,10,11);

  constructor() { }

  ngOnInit(): void {
  }

  changeEvent(event){
    // Return date object
    console.log(event.value._d);
  }

}
