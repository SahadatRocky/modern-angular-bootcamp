import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.css']
})
export class BadgeComponent implements OnInit {

  @Input() badgeNumber:string;
  @Input() badgePossition:any;
  @Input() label:string;
  @Input() badgeColor:any;
  constructor() { }

  ngOnInit(): void {
  }

}
