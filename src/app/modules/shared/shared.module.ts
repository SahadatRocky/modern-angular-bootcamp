import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { DividerComponent } from './divider/divider.component';
import { InputComponent as Ipcomponent } from './input/input.component';
import {InputComponent} from './components/input/input.component'
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { ButtonComponent } from './components/button/button.component';
import { IconComponent } from './components/icon/icon.component';
import { BadgeComponent } from './components/badge/badge.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { CardsComponent } from './components/cards/cards.component';
import { StepperComponent } from './components/stepper/stepper.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { AutocompleteComponent } from './components/autocomplete/autocomplete.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { RadioButtonComponent } from './components/radio-button/radio-button.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { SnackbarComponent } from './components/snackbar/snackbar.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { DialogviewComponent } from './components/dialog/dialogview/dialogview.component';
import { DataTableComponent } from './components/data-table/data-table.component';
import { SlideToggleComponent } from './components/slide-toggle/slide-toggle.component';
import { TimePickerComponent } from './components/time-picker/time-picker.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { PdfViewerComponent } from './components/pdf-viewer/pdf-viewer.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';

@NgModule({
  declarations: [
    DividerComponent,
    Ipcomponent,
    InputComponent,
    ButtonComponent,
    IconComponent,
    BadgeComponent,
    SpinnerComponent,
    ToolbarComponent,
    CardsComponent,
    StepperComponent,
    DropdownComponent,
    AutocompleteComponent,
    CheckboxComponent,
    RadioButtonComponent,
    DatePickerComponent,
    SnackbarComponent,
    DialogComponent,
    DialogviewComponent,
    DataTableComponent,
    SlideToggleComponent,
    TimePickerComponent,
    FileUploadComponent,
    PdfViewerComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedRoutingModule,
    AngularMaterialModule,
    NgxExtendedPdfViewerModule

  ],
  exports:[
    DividerComponent,
    Ipcomponent,
    InputComponent,
    ButtonComponent,
    IconComponent,
    BadgeComponent,
    SpinnerComponent,
    ToolbarComponent,
    CardsComponent,
    StepperComponent,
    DropdownComponent,
    AutocompleteComponent,
    CheckboxComponent,
    RadioButtonComponent,
    DatePickerComponent,
    SnackbarComponent,
    DialogComponent,
    DataTableComponent,
    SlideToggleComponent,
    TimePickerComponent,
    FileUploadComponent,
    PdfViewerComponent
  ]
})
export class SharedModule { }
