import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'app-input-sm',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  @Input() label:string;
  @Input() control: any;
  @Input() inputType:string;
  constructor() { }

  ngOnInit(): void {
  }

  showError(){
    const {dirty, touched, errors} = this.control;
    return dirty && touched && errors;
  }

}
