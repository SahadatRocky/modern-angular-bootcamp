import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-creditcard-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  @Input() control:any;
  @Input() label:any;

  constructor() { }

  ngOnInit(): void {
  }

  showError(){
    // const {dirty,touched,errors} = this.control;
    return this.control.dirty && this.control.touched && this.control.errors;
  }

}
