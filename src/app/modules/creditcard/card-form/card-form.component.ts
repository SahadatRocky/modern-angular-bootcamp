import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
  styleUrls: ['./card-form.component.css']
})
export class CardFormComponent implements OnInit {

  email:string = "";

  cardForm:any = FormGroup;

  constructor(private fb: FormBuilder) {
    this.createForm();
   }

   createForm(){
    this.cardForm = this.fb.group({
      name : new FormControl('',[Validators.required,Validators.minLength(3)]),
      cardNumber: new FormControl('',[Validators.required,Validators.minLength(16),Validators.maxLength(16)]),
      expiration: new FormControl('',[Validators.required,Validators.pattern(/^(0[1-9]|1[0-2])\/\d{2}$/)]),
      securityCode: new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(3)])
    });
   }



  ngOnInit(): void {
  }

  onSubmit(){
    console.log("form Submit...",this.cardForm.value);
  }

  onReset(){
    this.cardForm.reset();
  }

  templateOnSubmit(value:any){
      console.log(value);
  }

}
