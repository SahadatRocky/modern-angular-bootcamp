import { Component, OnInit } from '@angular/core';
import { PhotosService } from '../photos.service';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnInit {
  photoUrl: string = "";
  constructor(private photosService : PhotosService ) { }

  ngOnInit(): void {
    this.fetchPhoto();
  }

  onClick(){
    this.fetchPhoto();
  }


  fetchPhoto(){
      this.photosService.getPhoto().subscribe((response) => {
        //console.log(response.urls.regular);
        this.photoUrl = response.urls.regular;
    });
  }

}
