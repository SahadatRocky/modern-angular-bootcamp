import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewsRoutingModule } from './views-routing.module';
import { ViewHomeComponent } from './view-home/view-home.component';
import { StatisticComponent } from './statistic/statistic.component';
import { SharedModule } from '../shared/shared.module';
import { ItemListComponent } from './item-list/item-list.component';
import { PhotosComponent } from './photos/photos.component';


@NgModule({
  declarations: [
    ViewHomeComponent,
    StatisticComponent,
    ItemListComponent,
    PhotosComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ViewsRoutingModule

  ]
})
export class ViewsModule { }
