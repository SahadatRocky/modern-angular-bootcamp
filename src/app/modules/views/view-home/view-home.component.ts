import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-home',
  templateUrl: './view-home.component.html',
  styleUrls: ['./view-home.component.css']
})
export class ViewHomeComponent implements OnInit {
  stats = [
     {value: 22 ,label: "# of Users"},
     {value: 900 ,label: "Revenue"},
     {value: 50 ,label: "Reviews"}
  ];

  items = [
    {
      image: '/assets/c.jpg',
      title: 'Couch',
      description: 'This is a fantastic couch to sit on'
    }, {
      image: '/assets/d.jpg',
      title: 'Dresser',
      description: 'This is a great dresser to put stuff in'
    }

  ];

  constructor() { }

  ngOnInit(): void {
  }

}
