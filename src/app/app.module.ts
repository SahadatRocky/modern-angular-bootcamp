import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PasswordGenerateComponent } from './password-generate/password-generate.component';
import { CardComponent } from './card/card.component';
import { ExamplesPipeComponent } from './pipe/examples-pipe/examples-pipe.component';
import { ConvertPipe } from './pipe/convert.pipe';
import { PagesComponent } from './derective/pages/pages.component';
import { ClassDirective } from './derective/class.directive';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SharedRoutingModule } from './modules/shared/shared-routing.module';
import { SharedModule } from './modules/shared/shared.module';
import { AuthModule } from './modules/auth/auth.module';
import { AuthHttpInterceptor } from './modules/auth/auth-http.interceptor';
import { WeatherModule } from './modules/weather/weather.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [
    AppComponent,
    PasswordGenerateComponent,
    CardComponent,
    ExamplesPipeComponent,
    ConvertPipe,
    PagesComponent,
    ClassDirective,
    HomeComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedRoutingModule,
    AuthModule,
    WeatherModule,
    AppRoutingModule,
    MatMomentDateModule,
    SharedModule,
    ToastrModule.forRoot()

  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass:AuthHttpInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
