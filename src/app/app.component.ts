import { Component } from '@angular/core';
import { AuthService } from './modules/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'password-generator';
  posts = [{
    title: "Neat Tree",
    imageurl: "assets/tree.jpg",
    username: "nature",
    content: "I saw this neat tree today"

  },
  {
    title: "snowy Mountain",
    imageurl: "assets/mountain.jpg",
    username: "mountainLover",
    content: "Here is a picture of a snowy mountain"

  },
  {
    title: "biking cycle",
    imageurl: "assets/biking.jpg",
    username: "biking",
    content: "I did some biking today"

  }]

  signedin: boolean = false;
  constructor(private authService:AuthService){
       this.authService.signedin$.subscribe((signedin: boolean)=> {
           this.signedin = signedin;
       });
  }

  ngOnInit(){
      this.authService.checkAuth().subscribe(()=> {
        
      });
  }


}
