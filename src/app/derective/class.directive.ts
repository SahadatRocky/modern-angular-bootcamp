import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appClass]'
})
export class ClassDirective {

  @Input()
  set backgroundColor(color:string){
     console.log(color);
     this.element.nativeElement.style.backgroundColor = color;
  }


  constructor(private element: ElementRef) {
    console.log("class directive used");
   // this.element.nativeElement.style.backgroundColor = this.backgroundColor;
  }



/*
  ///for html -> [appClass]="{disabled : currentPage === 0}"
  @Input(appClass)
  set classNames(classObj:any){
    for(let key of classObj){
       if(classObj[key]){
          this.element.nativeElement.classList.add(key);
       }
       else{
         this.element.nativeElement.clasList.remove(key);
       }
    }
  }
  */

}
