import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamplesPipeComponent } from './examples-pipe.component';

describe('ExamplesPipeComponent', () => {
  let component: ExamplesPipeComponent;
  let fixture: ComponentFixture<ExamplesPipeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExamplesPipeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamplesPipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
