import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-examples-pipe',
  templateUrl: './examples-pipe.component.html',
  styleUrls: ['./examples-pipe.component.css']
})
export class ExamplesPipeComponent implements OnInit {
  name:string = "";
  date:string = "";
  amount:number = 0;
  height: number = 0;
  miles:number = 0;
  car = {
    mark: 'Toyota',
    model: 'camry',
    year: 2000
  };


  constructor() { }

  ngOnInit(): void {
  }

  onNameChange(value: any){
    this.name = value;
  }

  onDateChange(value:any){
    this.date = value;
  }

  onAmountChange(value:any){
    this.amount = parseFloat(value);
  }

  onHeightChange(value:any){
    this.height = parseFloat(value);
  }

  onMilesChange(value:any){
    this.miles = parseFloat(value);

  }
}
