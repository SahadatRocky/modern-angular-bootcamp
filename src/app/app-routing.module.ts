import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './modules/auth/auth.guard';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {
    path:'elements',
    loadChildren: () => import('../app/modules/elements/elements.module').then(m => m.ElementsModule)
  },
  {
    path:'collections',
    loadChildren: () => import('../app/modules/collections/collections.module').then(m => m.CollectionsModule)
  },
  {
    path: 'views',
    loadChildren: () => import('../app/modules/views/views.module').then(m => m.ViewsModule)
  },
  {
    path: 'mods',

    loadChildren: () => import('../app/modules/mods/mods.module').then(m => m.ModsModule)
  },
  {
    path: 'creditcard',
    loadChildren: () => import('../app/modules/creditcard/creditcard.module').then(m => m.CreditcardModule)
  },
  {
    path: 'inbox',
    canLoad: [AuthGuard],
    loadChildren: () => import('../app/modules/inbox/inbox.module').then(m=> m.InboxModule)
  },

  {
    path: 'data-transfer',
    loadChildren: () => import('../app/modules/data-transfer/data-transfer.module').then(m => m.DataTransferModule)
  },

  {
    path: 'dynamic',
    loadChildren: () => import('../app/modules/dynamic/dynamic.module').then(m => m.DynamicModule)
  },

  {
    path: 'javascript',
    loadChildren: () => import('../app/modules/javascript/javascript.module').then(m => m.JavascriptModule)
  },

  {
    path: 'rxjs',
    loadChildren: () => import('../app/modules/rxjs/rxjs.module').then(m => m.RxjsModule)
  },

  // {path:'home', component: HomeComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
