import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-password-generate',
  templateUrl: './password-generate.component.html',
  styleUrls: ['./password-generate.component.css']
})
export class PasswordGenerateComponent{

  title = 'password-generator';
  password = "";
  length = 0;
  includeLetters:boolean = false;
  includeNumbers:boolean = false;
  includeSymbols:boolean = false;

  generateBtnClick(){

     const numbers = '1234567890';
     const letters = 'abcdefghijklmnopqrstuvwxyz';
     const symbols = '!@#$%^&*()';

     let validChars = "";
     if(this.includeLetters){
       validChars += letters;
     }
     if(this.includeNumbers){
      validChars += numbers;
    }
    if(this.includeSymbols){
      validChars += symbols;
    }

    let generatedpassword = "";
    for(let i = 0; i<this.length; i++){
      const index = Math.floor(Math.random() * validChars.length );
      generatedpassword += validChars[index];
    }

    this.password = generatedpassword;
    // console.log(`
    //     about the generate password with the following-
    //     include letters-${this.UseLetters}
    //     include numbers- ${this.UseNumbers}
    //     include symbols- ${this.UseSymbols}
    //     `);
  }

  onChangeUseLetters(){
      this.includeLetters = !this.includeLetters;
  }

  onChangeUseNumbers(){
    this.includeNumbers = !this.includeNumbers;
  }

  onChangeUseSymbols(){
    this.includeSymbols = !this.includeSymbols;
  }

  onChnageLength($event: any){
    const parsedValue = parseInt($event.target.value);
      if(!isNaN(parsedValue)){
        this.length = parsedValue;
        console.log(this.length);
    }


  }

}
