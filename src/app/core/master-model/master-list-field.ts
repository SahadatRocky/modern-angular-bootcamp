import {MasterFormField} from './master-form-field';

export class MasterListField extends MasterFormField<any> {
  canShow: boolean;
  canSort: boolean;
  canTranslate: boolean;
  width: string;

  constructor(options: Partial<MasterListField> = {}) {
    super(options);
    this.canShow = options.canShow !== false;
    this.canSort = options.canSort === true;
    this.canTranslate = options.canTranslate === undefined ? true : options.canTranslate;
    this.width = options.width || null;
  }
}
