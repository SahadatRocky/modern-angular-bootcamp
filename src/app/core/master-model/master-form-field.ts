export class MasterFormField<T> {
  oid: string;
  value: T | T[];
  tempValue: T;
  key: string;
  name: string;
  nameEn: string;
  nameBn: string;
  id: string;
  label: string;
  listLabel: string;
  labelEn: string;
  labelBn: string;
  placeholder: string;
  controlType: string;
  fieldWidth: number;
  required: boolean;
  readOnly: boolean;
  visible: boolean;
  isDisabled: boolean;
  order: number;
  canSearch: boolean;
  canColor: boolean;
  type: any;
  editableInList: boolean;
  isDefaultValueSet: boolean;
  defaultFieldToSet?: string;
  allSelectable?: boolean;
  changeDetect: { valueToMatch?: any, keyToChange?: string, isFormValueChanged?: boolean, propertyToChange?: string, valueToChange?: any }[];
  isHiddenEventEmitter: boolean;
  toolTip?: string;
  clickable: boolean;
  textAlign: string;
  showToolTip?: boolean;
  textLimit?: number;

  constructor(options: Partial<MasterFormField<T>> = {}) {
    if (options.hasOwnProperty('name') === false) {
      throw Error('name must be specified');
    }
    if (options.hasOwnProperty('label') === false) {
      throw Error('label must be specified');
    }
    this.oid = options.oid;
    this.value = options.value;
    this.tempValue = options.tempValue;
    this.key = options.key || '';
    this.label = options.label;
    this.listLabel = options.listLabel || this.label;
    this.labelEn = options.labelEn;
    this.labelBn = options.labelBn;
    this.name = options.name;
    this.nameEn = options.nameEn;
    this.nameBn = options.nameBn;
    this.id = options.id || '';
    this.placeholder = options.placeholder || '';
    this.required = options.required === true;
    this.readOnly = options.readOnly || false;
    this.isDisabled = options.isDisabled !== false;
    this.order = options.order ? 1 : options.order;
    this.controlType = options.controlType || '';
    this.type = options.type || '';
    this.fieldWidth = options.fieldWidth;
    this.visible = options.visible !== false;
    this.canSearch = options.canSearch === true;
    this.editableInList = options.editableInList === true;
    this.isDefaultValueSet = options.isDefaultValueSet === true;
    this.defaultFieldToSet = options.defaultFieldToSet || '';
    this.allSelectable = options.allSelectable === true;
    this.changeDetect = options.changeDetect;
    this.isHiddenEventEmitter = options.isHiddenEventEmitter === true;
    this.toolTip = options.toolTip;
    this.canColor = options.canColor || false;
    this.clickable = options.clickable || false;
    this.textAlign = options.textAlign || null;
    this.showToolTip = options.showToolTip || false;
    this.textLimit = options.textLimit || 50;
  }
}

